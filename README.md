# S3 Bucket Creation with AWS CDK and CodeWhisperer

This project demonstrates the creation of an Amazon S3 bucket utilizing the AWS Cloud Development Kit (CDK) with the assistance of AWS CodeWhisperer for generating the CDK code. The S3 bucket is configured with essential properties such as versioning, lifecycle rule and encryption to ensure data durability and security.


## Requirements

* AWS CLI: Configured with appropriate access credentials.
* Node.js: Version 10.x or later.
* Python: Version 3.6 or later, along with pip.
* AWS CDK: Install the AWS CDK toolkit globally using npm.
* AWS CodeWhisperer: Access to AWS CodeWhisperer for generating CDK code snippets.


## Setup Instructions

### Step 1: Configure AWS CLI

Ensure your AWS CLI is configured with your credentials. Run aws configure to set up your access key, secret key, and default region.

### Step 2: Install Node.js and AWS CDK

If not already installed, download and install Node.js. Then, install the AWS CDK toolkit globally using npm:

`npm install -g aws-cdk`

### Step 3: Initialize CDK Project

Create a new directory for your project and navigate into it:

`cdk init app --language python`

### Step 4: Install Python Dependencies

Install the required Python dependencies, including the AWS CDK library for S3:

`pip install -r requirements.txt`

### Step 5: Use AWS CodeWhisperer for CDK Code

This project leverages AWS CodeWhisperer in VSCode to generate the required CDK code snippets for establishing an S3 bucket equipped with versioning and encryption. To optimize outcomes, begin your functions with descriptive comments, and CodeWhisperer will handle the remainder, crafting precise and functional code based on your specifications. 

### Step 6: Define the S3 Bucket in CDK

You can edit the [main stack file](my-cdk-project/my_cdk_project_stack.py) to define your S3 bucket. Here is a basic example:

    `from aws_cdk import (
        Duration,
        Stack,
        aws_s3 as s3,
        # aws_sqs as sqs,
    )
    from constructs import Construct`

    `class MyCdkProjectStack(Stack):

        def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
            super().__init__(scope, construct_id, **kwargs)
            
            bucket = s3.Bucket(self, "mybucket", bucket_name = 'my_cdk_project', versioned=True, encryption=s3.BucketEncryption.S3_MANAGED)
            
            bucket.add_lifecycle_rule(
                expiration=cdk.Duration.days(30)`

### Step 7: Deploy the CDK Stack

Deploy your CDK stack to AWS:

`cdk bootstrap` # incase your environment is not bootstraped

 `cdk deploy`

![image](img/deploy.png)


## Conclusion

This project sets up a secure and versioned S3 bucket using the AWS CDK, showcasing the integration of AWS services with infrastructure as code (IaC) practices. AWS CodeWhisperer aids in quickly generating secure and efficient infrastructure code, streamlining the development process.