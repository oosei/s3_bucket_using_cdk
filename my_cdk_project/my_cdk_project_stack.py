from aws_cdk import (
    Duration,
    Stack,
    aws_s3 as s3,
    # aws_sqs as sqs,
)
from constructs import Construct

class MyCdkProjectStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)
        
        bucket = s3.Bucket(self, "mybucket", bucket_name = 'my_cdk_project', versioned=True, encryption=s3.BucketEncryption.S3_MANAGED)
        
        bucket.add_lifecycle_rule(
            expiration=cdk.Duration.days(30)
            
    
